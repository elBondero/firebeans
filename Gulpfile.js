var gulp = require("gulp");
var sass = require("gulp-sass");
var rename = require("gulp-rename");
var argv = require("yargs").argv;


// - Automatically generate SASS files and build the extension for both firefox and chrome - //
gulp.task('default',["styles","build","watcher"]);


gulp.task('watcher',function() {
    gulp.watch('src/**',['styles','build']);
});


// - SASS - //
gulp.task("styles",function() {
  var targets = ["chrome","firefox"];
	if (argv.firefox) { targets = ["firefox"]; }
	if (argv.chrome) { targets = ["chrome"]; }


	targets.forEach(function(target) {
  	gulp.src("src/popup/sass/popup.scss")
  		.pipe(sass({includePaths: ["src/popup/sass"],outputStyle:"compressed"}).on('error', sass.logError))
  		.pipe(gulp.dest('dist/'+target+'/popup/css'));
  });
});

// - Build (you can limit the build process to one plattform by adding either  "--firefox" or "--chrome")
gulp.task("build",function() {
	// build for all target platforms by default
	var targets = ["chrome","firefox"];

	// only build for one target if requested
	if (argv.firefox) { targets = ["firefox"]; }
	if (argv.chrome) { targets = ["chrome"]; }

	targets.forEach(function(target) {
		// copy the plattform specific files
		gulp.src("src/manifest_"+target+".json")
			.pipe(rename({basename:"manifest",extname:".json"}))
			.pipe(gulp.dest("dist/"+target));

		// copy the universal files
		gulp.src(["src/**","!src/manifest_*.json"])
			.pipe(gulp.dest("dist/"+target));

    // copy fonts
    gulp.src(["bower_components/font-awesome/fonts/fontawesome-webfont.woff2","bower_components/source-sans-pro/fonts/source-sans-pro/source-sans-pro-bold.woff2","bower_components/source-sans-pro/fonts/source-sans-pro/source-sans-pro-regular.woff2","bower_components/source-sans-pro/fonts/source-sans-pro/source-sans-pro-semi-bold.woff2","bower_components/source-sans-pro/fonts/source-sans-pro/source-sans-pro-light.woff2"])
			.pipe(gulp.dest("dist/"+target+"/data/assets/fonts/"));

      // copy external libraries
      gulp.src(["bower_components/moment/min/moment.min.js","bower_components/moment/min/moment-with-locales.min.js","bower_components/moment/moment-timezome.js","bower_components/matchHeight/dist/jquery.matchHeight-min.js","bower_components/bluebird/js/browser/bluebird.min.js","bower_components/jquery/dist/jquery.min.js"])
        .pipe(gulp.dest("dist/"+target+"/lib/ext/"));

      // Copy Licenses file
      gulp.src("LICENSES")
        .pipe(gulp.dest("dist/"+target));

	});
});
