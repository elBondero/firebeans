/* --------------------------------------------------------------------------------------------
	

	BACKGROUND.JS
	

	Das Addon ist in zwei Hauptbestandteile eingeteilt:
	1. Das Background-Skript (diese Datei + background.html)
	2. Das Popup (/popup/popup.html + /popup/popup.js)

	Das Background-Skript ist das Hersztück, hier wird definiert, welche Seiten das Addon hat,
	hier werden alle Daten geholt, und alle Einstellungen gesetzt.

	Das Background-Skript läuft immer im Hintergrund, alle für das Plugin notwendigen
	Module (interne und externe Bibliotheken), werden im background.html geladen.

	Klickt der Benutzer auf das Addon-Icon in der Browserleiste, öffnet sich das Popup.
	Der Browser stellt dann im Prinzip in einem kleinen Fenster eine kleine Website da.
	Für jede Seite gibt es eigene Templates, die über die Datei popup.js geladen werden.

	Klickt der Benutzer auf einen Navigationspunkt im Popup, sendet das Popup.js einen Request
	und kommuniziert mit dem background.js (dieser Datei), dass es gerne eine bestimmte Seite
	öffnen würde. Wenn die angefragte Seite existiert, werden die für diese Seite notwendigen
	Daten geladen (z.B. Sendeplan-Daten für die Sendeplan-Seite). Sind alle für die Seite
	notwendigen Daten geladen, wird diese Daten durch einen Request an das Popup.js
	zurückgeschickt. Das Popup.js verarbeitet dann diese Daten und stellt diese im Popup
	mithilfe der verschiedenen Templates dar.

	So funktioniert im Prinzip das Addon. Zusätzlich gibt es dann noch Extra-Funktionen wie
	den Cache für verschiedene Arten von Daten, der vom Background.js gestartet und regelmäßig
	im Hintergrund ausgeführt wird.

	Im Popup.js finden sich wiederum auch JavaScript-Funktionen, die die UI betreffen (z.B.
	Handler, die bestimmen, was passieren soll, wenn bestimtme Links geklickt werden etc.)

------------------------------------------------------------------------------------------------ */

// Notwendige Module laden, damit später Daten über sie geholt werden können.
// Die Daten, in denen diese Module definiert sind, finden sich im ordner "/lib"
var youtube = new YouTube();
var blog = new Feed();
var reddit = new Reddit();
var sendeplan = new Sendeplan();
var options = new FirebeansOptions();




/*  ---------------------------------
				Panel Pages
	Damit die Seite später angesprochen, geladen und angezeigt
	werden können, wird hier zunächst definiert, welche Seiten
	im Addon angezeigt werden können.
	Diese Seiten müssen leider derzeit noch zusätzlich händisch
	in der Datei "popup.html" in das Seiten-Menü eingetragen werden
	--------------------------------- */

var pages = [
	{
		title: "Sendeplan",
		page: "sendeplan",
		template: "template-sendeplan.html",
		hideHeader: true
	},
	{
		title: "Blog",
		page: "blog",
		template: "template-blog.html",
	},
	{
		title: "Support",
		page: "support",
	},
	{
		title: "Links & Community",
		page: "communities"
	},
	{
		title: "Redditbeans",
		page: "reddit",
		subpages: [
			{
				title: "Beliebt",
				name: "reddit-hot"
			},
			{
				title: "Neu",
				name: "reddit-new"
			}
		]
	},
	{
		title: "Social Media Kram",
		page: "social"
	},
	{
		title: "Video on Demand",
		page: "vod",
		subpages: [
			{
				title: "Rocket Beans TV",
				name: "vod-rbtv"
			},
			{
				title: "RBTV Let's Play",
				name: "vod-rbtvlp"
			}
		]
	},
	{
		title: "Über Firebeans",
		page: "info",
		version: self.version,
		hideHeader: true
	},
	{
		title: "Playlists",
		page: "playlists",
		subpages: [
			{
				title: "Rocket Beans TV",
				name: "playlists-rbtv",
			},
			{
				title: "RBTV Let's Play",
				name: "playlists-rbtvlp",
			}
		]
	}
];

// Diese Funktion wird später benutzt, um allen beteiligten Funktionen
// mitteilen zu können, wie die Seite heißt und welche Unterseiten sie hat
function getPageInfo(target) {
	var foundItems = [];
	pages.forEach(function(item) {
		if (item.page === target) {
			foundItems.push(item);
		}
	});

	if (foundItems.length > 0) {
		return foundItems[0];
	}
	else {
		return false;
	}
}


/* ---- Getting and Sending Page Data ---- */
// 
chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
	if (request.title == "openPage" && typeof request.page !== "undefined") {
		var parameters = false;
		if (typeof request.parameters !== "undefined") {
			parameters = request.parameters;
		}

		if (typeof request.subpage !== "undefined") {
			chrome.runtime.sendMessage({
				"title": "startOpeningPage",
				"pageInfo": getPageInfo(request.page),
				"subpage": request.subpage,
				"parameters": parameters
			});
		}
		else {
			chrome.runtime.sendMessage({
				"title": "startOpeningPage",
				"pageInfo": getPageInfo(request.page),
				"parameters": parameters
			});
		}
	}
});


/* ---- Getting and Sending Page Data ---- */
chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
	if (request.title == "requestPageData" && typeof request.page !== "undefined") {

		/* -- Sendeplan -- */
		if (request.page == "sendeplan") {
			if (typeof request.parameters !== "undefined" && typeof request.parameters.date !== "undefined") {
				var timeStart = new Date(request.parameters.date);
				var timeEnd = new Date(request.parameters.date);
				timeEnd.setDate( timeEnd.getDate() + 1);
			}
			else {
				var timeStart = new Date();
				var timeEnd = new Date();
				timeEnd.setDate( timeEnd.getDate() + 1);
			}

			// normalize times
			timeStart.setHours(0);
			timeStart.setMinutes(0);
			timeStart.setSeconds(0);
			timeStart.setMilliseconds(0);
			timeEnd.setHours(0);
			timeEnd.setMinutes(0);
			timeEnd.setSeconds(0);
			timeEnd.setMilliseconds(0);

			sendeplan.getCachedEvents(function(cachedEvents) {
				chrome.runtime.sendMessage({
					title: "sendPageData",
					page: "sendeplan",
					pageInfo: getPageInfo("sendeplan"),
					data: cachedEvents.events,
					range: [timeStart.getTime(),timeEnd.getTime()],
					updated: cachedEvents.updated
				});
			},{
				filter: "range",
				range: [timeStart,timeEnd]
			});
		}

		/* -- Social -- */
		if (request.page == "social") {
			chrome.runtime.sendMessage({
				title: "sendPageData",
				page: "social",
				pageInfo: getPageInfo("social"),
				data: false
			});
		}

		/* -- Video on Demand -- */
		if (request.page == "vod") {

			var subpage = false;
			if (typeof request.subpage === "undefined") {
				var pageInfo = getPageInfo("vod"),
				subpage = pageInfo.subpages[0].name;
			}
			else {
				subpage = request.subpage;
			}

			if (subpage == "vod-rbtv") {
				youtube.getLatestChannelVideos("UCQvTDmHza8erxZqDkjQ4bQQ",function(thevideos) {
					chrome.runtime.sendMessage({
						title: "sendPageData",
						page: "vod",
						pageInfo: getPageInfo("vod"),
						data: {
							videos: thevideos,
							channelId: "UCQvTDmHza8erxZqDkjQ4bQQ"
						}
					});
				});
			}

			if (subpage == "vod-rbtvlp") {
				youtube.getLatestChannelVideos("UCtSP1OA6jO4quIGLae7Fb4g",function(thevideos) {
					chrome.runtime.sendMessage({
						title: "sendPageData",
						page: "vod",
						pageInfo: getPageInfo("vod"),
						data: {
							videos: thevideos,
							channelId: "UCtSP1OA6jO4quIGLae7Fb4g"
						}
					});
				});
			}

		}

		/* -- Blog -- */
		if (request.page == "blog") {
			blog.getFeed(function(items) {
				chrome.runtime.sendMessage({
					title: "sendPageData",
					page: "blog",
					pageInfo: getPageInfo("blog"),
					data: items
				});
			});
		}

		/* -- Support -- */
		if (request.page == "support") {

			var xhr = new XMLHttpRequest();
			xhr.open("GET", chrome.extension.getURL("data/links-support.json"), true);
			xhr.onreadystatechange = function() {
				if (xhr.readyState == 4) {
					var jaaasooon = JSON.parse(xhr.responseText);

					chrome.runtime.sendMessage({
						title: "sendPageData",
						page: "support",
						pageInfo: getPageInfo("support"),
						data: jaaasooon
					});
				}
			}
			xhr.send();

		}

		/* -- Community -- */
		if (request.page == "communities") {

			var xhr = new XMLHttpRequest();
			xhr.open("GET", chrome.extension.getURL("data/links-community.json"), true);
			xhr.onreadystatechange = function() {
				if (xhr.readyState == 4) {
					var jaaasooon = JSON.parse(xhr.responseText);

					chrome.runtime.sendMessage({
						title: "sendPageData",
						page: "communities",
						pageInfo: getPageInfo("communities"),
						data: jaaasooon
					});
				}
			}
			xhr.send();

		}

		/* -- Info -- */
		if (request.page == "info") {
			chrome.runtime.sendMessage({
				title: "sendPageData",
				page: "info",
				pageInfo: getPageInfo("info"),
				hideHeader: true,
				data: false
			});
		}

		/* -- Playlists -- */
		if (request.page == "playlists") {

			if (typeof request.subpage == "undefined" || request.subpage == "") {
				request.subpage = "playlists-rbtv";
			}

			var rbtvChannelId = false;
			if (request.subpage == "playlists-rbtv") { rbtvChannelId = "UCQvTDmHza8erxZqDkjQ4bQQ"; }
			else if (request.subpage == "playlists-rbtvlp") { rbtvChannelId = "UCtSP1OA6jO4quIGLae7Fb4g"; }

			chrome.storage.local.get("playlistsCache",function(playlistsCaches) {
				playlistsCaches = playlistsCaches.playlistsCache;

				if (typeof playlistsCaches !== "undefined") {
					if (playlistsCaches instanceof Array && playlistsCaches.length > 0) {
						playlistsCaches.forEach(function(playlistsCache) {
							if (playlistsCache.channelId == rbtvChannelId) {
								console.log("found cache",playlistsCache);
								chrome.runtime.sendMessage({
									title: "sendPageData",
									page: "playlists",
									subpage: request.subpage,
									pageInfo: getPageInfo("playlists"),
									data: playlistsCache.playlists
								});
							}
							else {
								debugLog("not the cache you're looking for");
							}
						});
					}
					else {
						debugLog("no cache");
						displayLoadingPage();
					}
				}
			});

			function displayLoadingPage() {
				chrome.runtime.sendMessage({
					title: "sendPageData",
					page: "playlists",
					subpage: request.subpage,
					pageInfo: getPageInfo("playlists"),
					data: "not-yet-cached"
				});
			}
		}

		/* -- Reddit -- */
		if (request.page == "reddit") {
			var subpage = false;
			if (typeof request.subpage === "undefined") {
				var pageInfo = getPageInfo("reddit"),
				subpage = pageInfo.subpages[0].name;
			}
			else {
				subpage = request.subpage;
			}

			if (subpage == "reddit-hot") {
				reddit.getLatestPosts("hot",function(response) {
					chrome.runtime.sendMessage({
						title: "sendPageData",
						page: "reddit",
						pageInfo: getPageInfo("reddit"),
						data: response
					});
				});
			}

			if (subpage == "reddit-new") {
				reddit.getLatestPosts("new",function(response) {
					chrome.runtime.sendMessage({
						title: "sendPageData",
						page: "reddit",
						pageInfo: getPageInfo("reddit"),
						data: response
					});
				});
			}
		}

	}
});


chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
	if (request.title == "requestPageDataInjection") {
		/*  -- Sendeplan -- */
		if (request.page == "sendeplan" && request.action == "getCurrentShow") {
			sendeplan.getCurrentEvent(function(currentEvent) {
				// set title & topic
				if (typeof currentEvent.title !== "undefined") {
					chrome.runtime.sendMessage({
						title: "injectPageData",
						target: ".twitch-header .title",
						page: "sendeplan",
						data: currentEvent.title
					});
				}
				if (typeof currentEvent.topic !== "undefined") {
					chrome.runtime.sendMessage({
						title: "injectPageData",
						target: ".twitch-header .subtitle",
						page: "sendeplan",
						data: currentEvent.topic
					});
				}

				// set game
				var game = "";
				if (typeof currentEvent.game !== "undefined") { game = currentEvent.game; }
				if (game != "") {
					chrome.runtime.sendMessage({
						title: "injectPageData",
						target: ".twitch-header .info .game .text",
						page: "sendeplan",
						data: currentEvent.game
					});
				}
				else {
					chrome.runtime.sendMessage({
						title: "injectPageData",
						target: ".twitch-header .info .game",
						page: "sendeplan",
						data: ""
					});
				}

				// set type
				if (typeof currentEvent.type !== "undefined") {
					if (currentEvent.type == "live") {
						chrome.runtime.sendMessage({
							title: "injectPageData",
							action: "addClass",
							target: ".twitch-header .live-status",
							page: "sendeplan",
							data: "live"
						});
						chrome.runtime.sendMessage({
							title: "injectPageData",
							action: "text",
							target: ".twitch-header .live-status",
							page: "sendeplan",
							data: "Live"
						});
					}
					if (currentEvent.type == "premiere") {
						chrome.runtime.sendMessage({
							title: "injectPageData",
							action: "addClass",
							target: ".twitch-header .live-status",
							page: "sendeplan",
							data: "premiere"
						});
						chrome.runtime.sendMessage({
							title: "injectPageData",
							action: "text",
							target: ".twitch-header .live-status",
							page: "sendeplan",
							data: "Neue Folge"
						});
					}
				}
			});
		}
	}
});


/*  ---------------------------------
				Caches
	--------------------------------- */

/* --------------------- Schedule Cache --------------------- */
function updateScheduleCache() {
	debugLog("updating schedule cache");
	sendeplan.updateCache(function() {
		// let the page reload when the cache was updated
		chrome.runtime.sendMessage({
			title: "scheduleCacheUpdated"
		});
	});
}

// - Update schedule cache when requested - //
chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
	if (request.title == "updateScheduleCache") {
		updateScheduleCache();
	}
});

// - Update schedule cache on install/update - //
if (typeof chrome.runtime.onInstalled !== "undefined") {
	chrome.runtime.onInstalled.addListener(function(details){
		if (details.reason == "install" || details.reason == "update") {
			updateScheduleCache();
		}
	});
}
else {
	// Firefox does not support onInstalled, so this function will fire on every startup until onInstalled is supported in Firefox
	updateScheduleCache();

}

// - Initiate the automatic schedule cache update system - //
if (typeof chrome.runtime.onInstalled !== "undefined") {
	chrome.runtime.onInstalled.addListener(function() {
		chrome.alarms.create("scheduleCacheUpdateAlarm",{delayInMinutes:1,periodInMinutes:240}); // 4 hours
	});
}
else {
	// Firefox does not support onInstalled, so the alarm will be created on every startup until onInstalled is supported in Firefox
	// check if the alarm has already been created or create one:

	chrome.alarms.getAll(function(alarms) {
		var alarmExists = false;
		alarms.forEach(function(alarm) {
			if (alarm.name == "scheduleCacheUpdateAlarm") {
				alarmExists = true;
			}
		});
		if (alarmExists == true) {
			chrome.alarms.create("scheduleCacheUpdateAlarm",{delayInMinutes:1,periodInMinutes:240}); // 4 hours
		}
	});

}

chrome.alarms.onAlarm.addListener(function(alarm) {
	if (alarm.name == "scheduleCacheUpdateAlarm") {
		updateScheduleCache();
	}
});





/* --------------------- Playlists Cache --------------------- *\
 *
 *	After installation and initial load of the addon, it will
 *	get all playlists for all channels, so they can be listed
 *	chronologically in the "Playlists" view.
 *	The actual contents of those playlists (the PlaylistItems)
 *	won't be loaded, they will be loaded when the user opens
 *	those individual playlists for the first time (this hasn't
 *	been implemented yet, though, a click on the Playlist will
 *	open the playlist in a new tab for the time being)
 *
 * ------------------------------------------------------------ */

function updatePlaylistsCaches() {
	debugLog("updating playlist cache");

	var rbtvChannels = ["UCQvTDmHza8erxZqDkjQ4bQQ","UCtSP1OA6jO4quIGLae7Fb4g"]

	youtube.updatePlaylistsCaches(rbtvChannels,function(newCache) {
	});
}

// update playlists cache on install/update
if (typeof chrome.runtime.onInstalled !== "undefined") {
	chrome.runtime.onInstalled.addListener(function(details){
		if (details.reason == "install") {
			updatePlaylistsCaches();
		}
	});
}
else {
	debugLog("firefox callback");
	updatePlaylistsCaches();
}

// initiate the playlists cache update system
if (typeof chrome.runtime.onInstalled !== "undefined") {
	chrome.runtime.onInstalled.addListener(function() {
		chrome.alarms.create("playlistsCacheUpdateAlarm",{when: Date.now() + 12,periodInMinutes:720}); // 12 hours
	});
}
else {
	// Firefox does not support onInstalled, so the alarm will be created on every startup until onInstalled is supported in Firefox
	// check if the alarm has already been created or create one:

	chrome.alarms.getAll(function(alarms) {
		var alarmExists = false;
		alarms.forEach(function(alarm) {
			if (alarm.name == "playlistsCacheUpdateAlarm") {
				alarmExists = true;
			}
		});
		if (alarmExists == true) {
			chrome.alarms.create("playlistsCacheUpdateAlarm",{delayInMinutes:1,periodInMinutes:720}); // 12 hours
		}
	});
}

chrome.alarms.onAlarm.addListener(function(alarm) {
	if (alarm.name == "playlistsCacheUpdateAlarm") {
		updatePlaylistsCaches();
	}
});


/* --------------------- Clear all Caches --------------------- */
chrome.runtime.onMessage.addListener(function(request,sender,sendResponse) {
	if (request.title == "clearCaches") {
		chrome.storage.local.clear(function() {
			chrome.runtime.sendMessage({
				title:"cachesCleared"
			});
		});
	}
});



/*  ---------------------------------
			Popup in a Tab
	--------------------------------- */
chrome.runtime.onMessage.addListener(function(request,sender,sendResponse) {
	if (request.title == "openPopupInTab") {
		openPopupInTab();
	}
});

// openPopupInTab();

function openPopupInTab() {
	chrome.tabs.create({
		url: chrome.extension.getURL("popup/popup.html")
	},function(tab) {
	});
}



/*  ---------------------------------
			Options
	--------------------------------- */
options.initiateOptions();




/*  ---------------------------------
	Debug: debugLog from popup
	to background
	--------------------------------- */
chrome.runtime.onMessage.addListener(function(request,sender,sendResponse) {
	if (request.title == "log") {
		debugLog(request.message);
	}
})
